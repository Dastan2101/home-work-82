const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require("../models/User");

const router = express.Router();

router.post('/', async (req, res) => {
    const token = req.get('Authorization');

    if (!token) {
        return res.status(401).send({error: "Authorization headers not present!"});
    }
    const user = await User.findOne({token});

    if (!user) {
        return res.status(401).send({error: "Token incorrect!"})
    }
    let dataHistory = req.body;

    dataHistory.user = user._id;
    dataHistory.datetime = new Date().toISOString();

    const historyTracks = new TrackHistory(dataHistory);

    historyTracks.save()
        .then(result => res.send(result))
        .catch(error => res.sendStatus(error))
});

module.exports = router;