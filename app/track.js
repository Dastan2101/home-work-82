const express = require('express');
const Track = require('../models/Track');
const Album = require('../models/Album');

const router = express.Router();

router.get('/',async (req, res) => {

    if (req.query.album) {
        await Track.find({album: req.query.album}).sort({number: 1}).populate("album")
            .then(track => res.send(track))
            .catch(() => res.sendStatus(500))
    } if (req.query.artist_id) {
        await Album.find({artist: req.query.artist_id}).sort({number: 1})
            .then(albums => {
                Promise.all(albums.map(album => Track.find({album: album._id})))
                    .then(result => res.send(result))
                    .catch(e => res.status(500).send(e))
            })
    } else {
        await Track.find()
            .then(track => res.send(track))
            .catch(() => res.sendStatus(500))
    }

});



module.exports = router;