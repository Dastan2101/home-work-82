const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);
    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const artist = await Artist.create(
        {title: 'Eminem', information: 'Marshall Bruce Mathers 3', image: 'Eminem.jpg'},
        {
            title: 'Akon',
            information: 'Aliaune Damala Bouga Time Bongo Puru Nacka Lu Lu Lu Badara Akon Thiam',
            image: 'Acon.jpg'
        },
        {title: 'Rihanna', information: 'Robyn Rihanna Fenty', image: 'Rihanna.jpeg'},
        {
            title: 'Jah Khalib',
            information: 'Бахтияр Мамедов',
            image: 'Jahkhalib.jpg'
        },
        {
            title: 'Imagine Dragons',
            information: 'Imagine Dragons is an American pop rock band from Las Vegas, Nevada, consisting of lead vocalist Dan Reynolds, lead guitarist Wayne Sermon, bassist Ben McKee, and drummer Daniel Platzman.',
            image: 'imaginedragons.jpg'
        }
    );

    const album = await Album.create(
        {
            title: 'Kamikaze',
            artist: artist[0]._id,
            yearOfIssue: 2017,
            image: 'Kamikaze.jpg'
        },
        {
            title: 'Infinite',
            artist: artist[0]._id,
            yearOfIssue: 2015,
            image: 'infinite.jpg'
        },
        {
            title: 'Revival',
            artist: artist[0]._id,
            yearOfIssue: 2016,
            image: 'Revival.jpg'
        },
        {
            title: 'Freedom',
            artist: artist[1]._id,
            yearOfIssue: 2008,
            image: 'Freedom.jpg'
        },
        {
            title: 'Trouble',
            artist: artist[1]._id,
            yearOfIssue: 2004,
            image: 'Trouble.jpg'
        },
        {
            title: 'In Your Eyes',
            artist: artist[1]._id,
            yearOfIssue: 2005,
            image: 'InYourEyes.jpg'
        },
        {
            title: 'Music of the Sun',
            artist: artist[2]._id,
            yearOfIssue: 2005,
            image: 'Music_of_the_Sun.jpg'
        },
        {
            title: 'Music of Night',
            artist: artist[2]._id,
            yearOfIssue: 2007,
            image: 'musicOfTheNight.jpg'
        },
        {
            title: 'Всё, что мы любим',
            artist: artist[3]._id,
            yearOfIssue: 2016,
            image: 'vsechtomy.jpg'
        },
        {
            title: 'Джазовый грув',
            artist: artist[3]._id,
            yearOfIssue: 2017,
            image: 'jazzOviy.jpg'
        },
        {
            title: 'Origins',
            artist: artist[4]._id,
            yearOfIssue: 2014,
            image: 'Origins.jpg'
        },
        {
            title: 'Night Visions',
            artist: artist[4]._id,
            yearOfIssue: 2018,
            image: 'NightVisions.jpg'
        }
    );


    await Track.create(
        {
            title: 'The Ringer',
            album: album[0]._id,
            duration: '5:37',
            number: 1
        },
        {
            title: 'The drink',
            album: album[0]._id,
            duration: '4:37',
            number: 2
        },
        {
            title: 'Hello, Guys',
            album: album[0]._id,
            duration: '5:02',
            number: 3
        },
        {
            title: 'Walk on Water',
            album: album[1]._id,
            duration: '5:04',
            number: 1
        },
        {
            title: 'Walk on street',
            album: album[1]._id,
            duration: '5:04',
            number: 2
        },
        {
            title: 'What a f?',
            album: album[1]._id,
            duration: '3:04',
            number: 3
        },
        {
            title: 'Greatest',
            album: album[2]._id,
            duration: '3:46',
            number: 1
        },
        {
            title: 'Breeze',
            album: album[2]._id,
            duration: '3:16',
            number: 2
        },
        {
            title: 'Right Now (Na Na Na)',
            album: album[3]._id,
            duration: '3:95',
            number: 1
        },
        {
            title: 'La la la la',
            album: album[3]._id,
            duration: '3:65',
            number: 2
        },
        {
            title: 'Beautiful',
            album: album[4]._id,
            duration: '4:01',
            number: 1
        },
        {
            title: 'Moon',
            album: album[4]._id,
            duration: '4:31',
            number: 2
        },
        {
            title: 'Pon de Replay',
            album: album[5]._id,
            duration: '4:06',
            number: 1
        },
        {
            title: 'Put it up',
            album: album[5]._id,
            duration: '5:12',
            number: 2
        },
        {
            title: 'Here I Go Again',
            album: album[6]._id,
            duration: '4:11',
            number: 1
        },
        {
            title: 'I am here without',
            album: album[6]._id,
            duration: '4:43',
            number: 2
        },
        {
            title: 'When we go',
            album: album[7]._id,
            duration: '2:11',
            number: 1
        },
        {
            title: 'Here I Go Again',
            album: album[7]._id,
            duration: '2:11',
            number: 2
        },
        {
            title: 'What are you?',
            album: album[8]._id,
            duration: '4:11',
            number: 1
        },
        {
            title: 'Who are you?',
            album: album[8]._id,
            duration: '4:43',
            number: 2
        },
        {
            title: 'Again',
            album: album[9]._id,
            duration: '2:45',
            number: 1
        },
        {
            title: 'So sweet',
            album: album[9]._id,
            duration: '4:32',
            number: 2
        },
        {
            title: 'So cool',
            album: album[10]._id,
            duration: '5:32',
            number: 1
        },
        {
            title: 'Guid',
            album: album[10]._id,
            duration: '5:21',
            number: 2
        },
        {
            title: 'Come with me',
            album: album[11]._id,
            duration: '3:12',
            number: 1
        },
        {
            title: 'When you will come with me',
            album: album[11]._id,
            duration: '3:65',
            number: 2
        }
    );

    await connection.close();

};

run().catch(error => {
    console.error('Something went', error);
});